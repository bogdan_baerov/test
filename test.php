<?php
$ipArray = [];
$dateArray = [];
$methodArray = [];
$urlArray = [];
$userAgentArray = [];

foreach (glob("logs/*.log") as $filename) {
    $fn = fopen($filename, "r");
    while(!feof($fn))  {
        $string = fgets($fn);
        $resultIp = preg_match("/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/", $string, $matchesIp);
        $resultDate = preg_match("/\[(\d{1,2}\/\w{3}\/\d{4})/", $string, $matchesDate);
        $resultMethod = preg_match("/\] \"(\w{1,7})/", $string, $matchesMethod);
        $resultUrl = preg_match("/(http|https|^\/)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", $string, $matchesUrl);
        $resultUserAgent = preg_match("/(MSIE|Trident|(?!Gecko.+)Firefox|(?!AppleWebKit.+Chrome.+)Safari(?!.+Edge)|(?!AppleWebKit.+)Chrome(?!.+Edge)|(?!AppleWebKit.+Chrome.+Safari.+)Edge|AppleWebKit(?!.+Chrome|.+Safari)|Gecko(?!.+Firefox))(?: |\/)([\d\.apre]+)/", $string, $matchesUserAgent);

        // $result = preg_match("/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).+\[(\d{1,2}\/\w{3}\/\d{4}).+\] \"(\w{1,7}).+ \"(http.+|\/\w)\" \"(.+)\" /", $string, $matches);
        if($resultIp){
            $ipArray[] = $matchesIp[0];
        }else{
            $ipArray[] = "Nedefinit/neexistent in log";
        }
        if($resultDate){
            $matchesDate[0] = preg_replace("/^\[/", "", $matchesDate[0]);
            $dateArray[] = $matchesDate[0];
        }else{
            $dateArray[] = "Nedefinit/neexistent in log";
        }
        if($resultMethod){
            $matchesMethod[0] = preg_replace("/^\] \"/", "", $matchesMethod[0]);
            $methodArray[] = $matchesMethod[0];
        }else{
            $methodArray[] = "Nedefinit/neexistent in log";
        }
        if($resultUrl){
            $matchesUrl[0] = str_replace("\"", "", $matchesUrl[0]);
            $urlArray[] = $matchesUrl[0];
        }else{
            $urlArray[] = "Nedefinit/neexistent in log";
        }
        if($resultUserAgent){
            $userAgentArray[] = $matchesUserAgent[0];
        }else{
            $userAgentArray[] = "Nedefinit/neexistent in log";
        }
    }
    fclose($fn);
}
echo "IP addresses: ";
print_r($ipArray);
echo "\n---------\n";
echo "Days: ";
print_r($dateArray);
echo "\n---------\n";
echo "Request methods: ";
print_r($methodArray);
echo "\n---------\n";
echo "Urls: ";
print_r($urlArray);
echo "\n---------\n";
echo "User agents: ";
print_r($userAgentArray);
?>