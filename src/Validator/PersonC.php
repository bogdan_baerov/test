<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PersonC extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message; // validation message
    public $limit; // integer, limit of how many fields can have the same value
    public $timeInterval; // string, time interval limit, "hours" can be changed with seconds, minutes, hours, weeks, months, years
    public $customField; // string, the field that the constraint is put on in the validator query
    public $dateField; // string, the date field that is included in the validator query
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
    public function validatedBy()
    {
        return static::class.'Validator';
    }
}
