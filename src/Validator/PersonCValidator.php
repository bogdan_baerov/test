<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Doctrine\ORM\EntityManagerInterface;


class PersonCValidator extends ConstraintValidator
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\PersonC */

        if (null === $constraint->limit || null === $constraint->timeInterval || null === $constraint->customField || null === $constraint->dateField) {
            return;
        }
        if (null === $value || "" === $value) {
            return;
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $customFieldValue = $propertyAccessor->getValue($value, $constraint->customField);

        $qb = $this->em->createQueryBuilder("p.{$constraint->customField}");
        $qb
            ->select($qb->expr()->count("p.{$constraint->customField}"))
            ->from(get_class($value), "p")
            ->where("p.{$constraint->dateField} >= :date")
            ->andWhere("p.{$constraint->customField} = :field")
            ->groupBy("p.{$constraint->customField}")
            ->setParameter(":field", $customFieldValue)
            ->setParameter(":date", new \DateTime($constraint->timeInterval));

        $query = $qb->getQuery();
        $qResultArray = $query->getArrayResult();

        if(!empty($qResultArray[0][1]) &&  $qResultArray[0][1] >= $constraint->limit)
        {
            $this->context->buildViolation($constraint->message)
            ->atPath($constraint->customField)
            ->setParameter("{{ customFieldValue }}", $customFieldValue)
            ->setParameter("{{ dateField }}", $constraint->dateField)
            ->setParameter("{{ limit }}", $constraint->limit)
            ->setParameter("{{ timeInterval }}", $constraint->timeInterval)
            ->addViolation();
            return;
        }
    }
}
