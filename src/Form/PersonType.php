<?php

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Sex', ChoiceType::class, [
                'choices'  => [
                    'Masculin' => 'M',
                    'Feminin' => 'F',
                ],
            ])
            ->add('Nume')
            ->add('Prenume')
            ->add('Adresa')
            ->add('Oras')
            ->add('Email')
            ->add('Termeni')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'csrf_protection' => false
        ]);
    }
}