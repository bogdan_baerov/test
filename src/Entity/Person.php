<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as PersonAssert;


/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @PersonAssert\PersonC(
 *      message="Valoarea '{{ customFieldValue }}' a mai fost folosita în ultima ora. Limita este de {{ limit }} inregistrari pe ora",
 *      limit=3, 
 *      timeInterval="-1 hours",
 *      customField="Adresa",
 *      dateField="createdAt",
 * )
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $Sex;

    /**
     * @ORM\Column(type="string", length=19)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9]*$/",
     *     message="Numele trebuie sa contina doar caractere alfanumerice!"
     * )      
     */
    private $Nume;

    /**
     * @ORM\Column(type="string", length=14)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9]*$/",
     *     message="Prenumele trebuie sa contina doar caractere alfanumerice!"
     * )    
     */
    private $Prenume;

    /**
     * @ORM\Column(type="string", length=38)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9-,.;’ ]+$/",
     *     message="Adresa trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-', ',', '.', ';' si '’' !"
     * )    
     */
    private $Adresa;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z0-9-,.;’ ]+$/",
     *     message="Orasul trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-' !"
     * ) 
     */
    private $Oras;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Email(
     *     message = "Emailul '{{ value }}' nu este un mail valid. Introduceti un email valid!"
     * )
     */
    private $Email;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\EqualTo(
     *     value = 1,
     *     message = "Va rugam sa acceptati termenii si conditiile inainte de a da submit!"
     * )
     */
    private $Termeni;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSex(): ?string
    {
        return $this->Sex;
    }

    public function setSex(string $Sex): self
    {
        $this->Sex = $Sex;

        return $this;
    }

    public function getNume(): ?string
    {
        return $this->Nume;
    }

    public function setNume(string $Nume): self
    {
        $this->Nume = $Nume;

        return $this;
    }

    public function getPrenume(): ?string
    {
        return $this->Prenume;
    }

    public function setPrenume(string $Prenume): self
    {
        $this->Prenume = $Prenume;

        return $this;
    }

    public function getAdresa(): ?string
    {
        return $this->Adresa;
    }

    public function setAdresa(string $Adresa): self
    {
        $this->Adresa = $Adresa;

        return $this;
    }

    public function getOras(): ?string
    {
        return $this->Oras;
    }

    public function setOras(string $Oras): self
    {
        $this->Oras = $Oras;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getTermeni(): ?bool
    {
        return $this->Termeni;
    }

    public function setTermeni(bool $Termeni): self
    {
        $this->Termeni = $Termeni;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
    * @ORM\PrePersist
    * @ORM\PreUpdate
    */
    public function setUpdatedAtValue()
    {
        $this->setUpdatedAt(new \DateTime());    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime());
        }
    }

    public function toArray()
    {
        return [
            'Id' => $this->getId(),
            'Sex' => $this->getSex(),
            'Nume' => $this->getNume(),
            'Prenume' => $this->getPrenume(),
            'Adresa' => $this->getAdresa(),
            'Oras' => $this->getOras(),
            'Email' => $this->getEmail(),
            'Termeni' => $this->getTermeni()
        ];
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}