<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator as PersonAssert;

use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/person")
 */
class PersonController extends AbstractController
{

    private $validator;

    public function __construct(PersonRepository $personRepository, ValidatorInterface $validator)
    {
        $this->personRepository = $personRepository;
        $this->validator = $validator;
    }

    /**
     * @Route("/add", name="person_register_rest", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $validator = $this->validator;

        $data = json_decode($request->getContent(), true);

        $sex = $data['Sex'];
        $nume = $data['Nume'];
        $prenume = $data['Prenume'];
        $adresa = $data['Adresa'];
        $oras = $data['Oras'];
        $email = $data['Email'];
        $termeni = $data['Termeni'];

        $groups = new Assert\GroupSequence(['Default', 'Person']);

        $constraint = [
            new PersonAssert\PersonC([
                'message' => "Valoarea '{{ customFieldValue }}' a mai fost folosita în ultima ora. Limita este de {{ limit }} inregistrari pe ora",
                'limit' => 3, 
                'timeInterval' => "-1 hours",
                'customField' => "Adresa",
                'dateField' => "createdAt",
            ]),
            new Assert\Collection([
            'Sex' => [
                new Assert\NotNull(['message' => "Campul 'Sex' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Sex' este obligatoriu!"]),
                new Assert\Length(['max' => 1, 'maxMessage' => "Sexul trebuie sa aiba maxim 1 caracter"]),
                new Assert\Regex(['pattern' => "/[MF]/", 'message' => "Sexul trebuie sa contina 'M' sau 'F'"]),
            ],
            'Nume' => [
                new Assert\NotNull(['message' => "Campul 'Nume' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Nume' este obligatoriu!"]),
                new Assert\Length(['max' => 19, 'maxMessage' => 'Numele trebuie sa aiba maxim 19 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9]*$/', 'message' => 'Numele trebuie sa contina doar caractere alfanumerice!']),
            ],
            'Prenume' => [
                new Assert\NotNull(['message' => "Campul 'Prenume' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Prenume' este obligatoriu!"]),
                new Assert\Length(['max' => 14, 'maxMessage' => 'Prenumele trebuie sa aiba maxim 14 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9]*$/', 'message' => 'Prenumele trebuie sa contina doar caractere alfanumerice!'])
            ],
            'Adresa' => [
                new Assert\NotNull(['message' => "Campul 'Adresa' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Adresa' este obligatoriu!"]),
                new Assert\Length(['max' => 38, 'maxMessage' => 'Adresa trebuie sa aiba maxim 38 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9-,.;’ ]+$/', 'message' => "Adresa trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-', ',', '.', ';' si '’' !"]),
            ],
            'Oras' => [
                new Assert\NotNull(['message' => "Campul 'Oras' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Oras' este obligatoriu!"]),
                new Assert\Length(['max' => 32, 'maxMessage' => 'Orasul trebuie sa aiba maxim 32 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9-,.;’ ]+$/', 'message' => "Orasul trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-' !"]),
            ],
            'Email' => [
                new Assert\NotNull(['message' => "Campul 'Email' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Email' este obligatoriu!"]),
                new Assert\Length(['max' => 20, 'maxMessage' => 'Emailul trebuie sa aiba maxim 20 caractere!']),
                new Assert\Regex(['pattern' => "/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/", 'message' => "Emailul nu este un mail valid. Introduceti un email valid!"]),
            ],
            'Termeni' => [
                new Assert\NotNull(['message' => "Campul 'Termeni' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Termeni' este obligatoriu!"]),
                new Assert\EqualTo(['value' => 1, 'message' => "Va rugam sa acceptati termenii si conditiile inainte de a da submit!"]),
            ],
        ])];

        $violations = $validator->validate($data, $constraint, $groups);

        $violationsMessages = "";

        foreach($violations as $violation){
            $violationsMessages .= $violation;
        }

        $messagesArray = explode("Array", $violationsMessages);
        array_shift($messagesArray);

        if(empty($messagesArray)){
            $this->personRepository->registerPerson($sex, $nume, $prenume, $adresa, $oras, $email, $termeni);
        }else{
            return new JsonResponse($messagesArray, Response::HTTP_OK);
        }
        return new JsonResponse(['status' => 'Persoana a fost înregistrată!'], Response::HTTP_CREATED);
    }

     /**
     * @Route("/get/{id}", name="person_display_rest", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        $person = $this->personRepository->findOneBy(['id' => $id]);

        $data = [
            'Id' => $person->getId(),
            'Sex' => $person->getSex(),
            'Nume' => $person->getNume(),
            'Prenume' => $person->getPrenume(),
            'Adresa' => $person->getAdresa(),
            'Oras' => $person->getOras(),
            'Email' => $person->getEmail(),
            'Termeni' => $person->getTermeni(),
        ];
    
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/update/{id}", name="person_update_rest", methods={"PUT"})
     */
    public function update($id, Request $request): JsonResponse
    {
        $validator = $this->validator;

        $person = $this->personRepository->findOneBy(['id' => $id]);
        $data = json_decode($request->getContent(), true);

        empty($data['Sex']) ? true : $person->setSex($data['Sex']);
        empty($data['Nume']) ? true : $person->setNume($data['Nume']);
        empty($data['Prenume']) ? true : $person->setPrenume($data['Prenume']);
        empty($data['Adresa']) ? true : $person->setAdresa($data['Adresa']);
        empty($data['Oras']) ? true : $person->setOras($data['Oras']);
        empty($data['Email']) ? true : $person->setEmail($data['Email']);
        empty($data['Termeni']) ? true : $person->setTermeni($data['Termeni']);

        $groups = new Assert\GroupSequence(['Default', 'Person']);

        $constraint = new Assert\Collection([
            'Sex' => [
                new Assert\NotNull(['message' => "Campul 'Sex' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Sex' este obligatoriu!"]),
                new Assert\Length(['max' => 1, 'maxMessage' => "Sexul trebuie sa aiba maxim 1 caracter"]),
                new Assert\Regex(['pattern' => "/[MF]/", 'message' => "Sexul trebuie sa contina 'M' sau 'F'"]),
            ],
            'Nume' => [
                new Assert\NotNull(['message' => "Campul 'Nume' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Nume' este obligatoriu!"]),
                new Assert\Length(['max' => 19, 'maxMessage' => 'Numele trebuie sa aiba maxim 19 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9]*$/', 'message' => 'Numele trebuie sa contina doar caractere alfanumerice!']),
            ],
            'Prenume' => [
                new Assert\NotNull(['message' => "Campul 'Prenume' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Prenume' este obligatoriu!"]),
                new Assert\Length(['max' => 14, 'maxMessage' => 'Prenumele trebuie sa aiba maxim 14 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9]*$/', 'message' => 'Prenumele trebuie sa contina doar caractere alfanumerice!'])
            ],
            'Adresa' => [
                new Assert\NotNull(['message' => "Campul 'Adresa' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Adresa' este obligatoriu!"]),
                new Assert\Length(['max' => 38, 'maxMessage' => 'Adresa trebuie sa aiba maxim 38 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9-,.;’ ]+$/', 'message' => "Adresa trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-', ',', '.', ';' si '’' !"]),
            ],
            'Oras' => [
                new Assert\NotNull(['message' => "Campul 'Oras' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Oras' este obligatoriu!"]),
                new Assert\Length(['max' => 32, 'maxMessage' => 'Orasul trebuie sa aiba maxim 32 caractere!']),
                new Assert\Regex(['pattern' => '/^[a-zA-Z0-9-,.;’ ]+$/', 'message' => "Orasul trebuie sa contina doar caractere alfanumerice, spatiu si caracterele '-' !"]),
            ],
            'Email' => [
                new Assert\NotNull(['message' => "Campul 'Email' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Email' este obligatoriu!"]),
                new Assert\Length(['max' => 20, 'maxMessage' => 'Emailul trebuie sa aiba maxim 20 caractere!']),
                new Assert\Regex(['pattern' => "/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/", 'message' => "Emailul nu este un mail valid. Introduceti un email valid!"]),
            ],
            'Termeni' => [
                new Assert\NotNull(['message' => "Campul 'Termeni' este obligatoriu!"]),
                new Assert\NotBlank(['message' => "Campul 'Termeni' este obligatoriu!"]),
                new Assert\EqualTo(['value' => 1, 'message' => "Va rugam sa acceptati termenii si conditiile inainte de a da submit!"]),
            ]
        ]);

        $violations = $validator->validate($data, $constraint, $groups);

        $violationsMessages = "";

        foreach($violations as $violation){
            $violationsMessages .= $violation;
        }

        $messagesArray = explode("Array", $violationsMessages);
        array_shift($messagesArray);

        if(empty($messagesArray)){
            $updatedPerson = $this->personRepository->updatePersonInfo($person);
        }else{
            return new JsonResponse($messagesArray, Response::HTTP_OK);
        }
        return new JsonResponse($updatedPerson->toArray(), Response::HTTP_OK);
    }

     /**
     * @Route("/remove/{id}", name="remove_person_rest", methods={"DELETE"})
     */
    public function remove($id, Request $request): JsonResponse
    {
        $person = $this->personRepository->findOneBy(['id' => $id]);

        $this->personRepository->removePerson($person);
    
        return new JsonResponse(['status' => 'Person deleted'], Response::HTTP_NO_CONTENT);
    }


    /**
     * @Route("/", name="person_index", methods={"GET"})
     */
    public function index(PersonRepository $personRepository): Response
    {
        return $this->render('person/index.html.twig', [
            'people' => $personRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="person_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($person);
            $entityManager->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_show", methods={"GET"})
     */
    public function show(Person $person): Response
    {
        return $this->render('person/show.html.twig', [
            'person' => $person,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="person_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->isCsrfTokenValid('delete'.$person->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();
        }

        return $this->redirectToRoute('person_index');
    }
}
