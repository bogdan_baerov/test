<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\Logging;
use Doctrine\ORM\EntityManagerInterface;

class BatchLogCommand extends Command
{
    protected static $defaultName = 'app:batch-log';

    protected $em;

    public function __construct(EntityManagerInterface $manager)
    {
        parent::__construct();
        $this->em = $manager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $batchSize = 200;
        $counter = 0;
        foreach (glob("logs/*.log") as $filename) {
            
            $fn = fopen($filename, "r");
            while(!feof($fn))  {
                $string = fgets($fn);
                $log = new Logging();

                $string = fgets($fn);
                $resultIp = preg_match("/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/", $string, $matchesIp);
                $resultDate = preg_match("/\[(\d{1,2}\/\w{3}\/\d{4})/", $string, $matchesDate);
                $resultMethod = preg_match("/\] \"(\w{1,7})/", $string, $matchesMethod);
                $resultUrl = preg_match("/(http|https|^\/)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/", $string, $matchesUrl);
                $resultUserAgent = preg_match("/(MSIE|Trident|(?!Gecko.+)Firefox|(?!AppleWebKit.+Chrome.+)Safari(?!.+Edge)|(?!AppleWebKit.+)Chrome(?!.+Edge)|(?!AppleWebKit.+Chrome.+Safari.+)Edge|AppleWebKit(?!.+Chrome|.+Safari)|Gecko(?!.+Firefox))(?: |\/)([\d\.apre]+)/", $string, $matchesUserAgent);
                $counter++;
                if($resultIp){
                    $log->setIp($matchesIp[0]);
                    $output->writeln($matchesIp[0]);
                }else{
                    $log->setIp("Nedefinit/neexistent in log");
                }
                if($resultDate){
                    $matchesDate[0] = preg_replace("/^\[/", "", $matchesDate[0]);
                    $log->setDate($matchesDate[0]);
                    $output->writeln($matchesDate[0]);
                }else{
                    $log->setDate("Nedefinit/neexistent in log");
                }
                if($resultMethod){
                    $matchesMethod[0] = preg_replace("/^\] \"/", "", $matchesMethod[0]);
                    $log->setHttpMethod($matchesMethod[0]);
                    $output->writeln($matchesMethod[0]);
                }else{
                    $log->setHttpMethod("Nedefinit/neexistent in log");
                }
                if($resultUrl){
                    $matchesUrl[0] = str_replace("\"", "", $matchesUrl[0]);
                    $log->setURL($matchesUrl[0]);
                    $output->writeln($matchesUrl[0]);
                }else{
                    $log->setURL("Nedefinit/neexistent in log");
                }
                if($resultUserAgent){
                    $log->setUserAgent($matchesUserAgent[0]);
                    $output->writeln($matchesUserAgent[0]);
                }else{
                    $log->setUserAgent("Nedefinit/neexistent in log");
                }
                $output->writeln("-----");
                $this->em->persist($log);
                if(($counter % $batchSize === 0)){
                    $this->em->flush();
                    $this->em->clear();
                }
            }
            fclose($fn);
        }
        $this->em->flush(); //Persist objects that did not make up an entire batch
        $this->em->clear();

        return Command::SUCCESS;
    }
}
