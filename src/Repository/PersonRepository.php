<?php

namespace App\Repository;

use App\Entity\Person;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Person::class);
        $this->manager = $manager;
    }

    public function registerPerson($sex, $nume, $prenume, $adresa, $oras, $email, $termeni)
    {
        $newPerson = new Person();

        $newPerson       
            ->setSex($sex)
            ->setNume($nume)
            ->setPrenume($prenume)
            ->setAdresa($adresa)
            ->setOras($oras)
            ->setEmail($email)
            ->setTermeni($termeni);

        $this->manager->persist($newPerson);
        $this->manager->flush();
    }

    public function updatePersonInfo(Person $person): Person
    {
        $this->manager->persist($person);
        $this->manager->flush();

        return $person;
    }

    public function removePerson(Person $person)
    {
        $this->manager->remove($person);
        $this->manager->flush();
    }
}
