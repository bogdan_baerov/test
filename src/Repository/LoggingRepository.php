<?php

namespace App\Repository;

use App\Entity\Logging;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Logging|null find($id, $lockMode = null, $lockVersion = null)
 * @method Logging|null findOneBy(array $criteria, array $orderBy = null)
 * @method Logging[]    findAll()
 * @method Logging[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoggingRepository extends ServiceEntityRepository
{
    private $manager;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Logging::class);
        $this->manager = $manager;
    }

    public function registerLog($ip, $date, $httpMethod, $url, $userAgent)
    {
        $newLog = new Logging();

        $newLog       
            ->setIp($ip)
            ->setDate($date)
            ->setHttpMethod($httpMethod)
            ->setUrl($url)
            ->setUserAgent($userAgent);

        $this->manager->persist($newLog);
        $this->manager->flush();
    }
}
